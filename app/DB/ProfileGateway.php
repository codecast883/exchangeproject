<?php
require_once 'DB.php' ;
require_once 'Profile.php' ;


class ProfileGateway{
	private $db;
	public $data;

	public function __construct(){
		global $dbo;
		$this->db = $dbo;
		$this->data = new Profile;
		
	}


	public function findAll(){
		$sql = 'SELECT * FROM ' . 'student';
		return $this->db->query($sql);
	}


	public function addedUser($array){
		$sql = "INSERT INTO users (user_id, first_name, last_name, second_name, email, telephone_number, city, login, pass) VALUES (:userId, :firstName, :lastName, :secondName, :email, :telephoneNumber, :city, :login, :password)";
		$statement = $this->db->dbh->prepare($sql);

		foreach ($array as $key => $value) {
			$statement->bindValue(':'. $key, $value);
		}

		$statement->execute();
		return true;

	}



	public function findByHash($hash){
		$sql = "SELECT * FROM student WHERE pass_hash = '$hash'";
		return $this->db->query($sql);
		
	}


}
?>