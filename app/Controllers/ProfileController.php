<?php
	require_once ROOT . '/../app/DB/ProfileGateway.php' ;
	require_once ROOT . '/../app/Components/Validator.php' ;
	class ProfileController{
		public $date;
		private $model;
		private $validator; 
		
		public function __construct(){
			$this->model = new ProfileGateway;
			$this->date = new DateTime();
			$this->validator = new Validator();

		}


		public function actionCreate(){

			$dataArr = [];
			$err = [];

			foreach ($this->model->data as $key => $value) {
				$err[0][$key] = '';
				$err[1][$key] = '';
				$dataArr[$key] = '';
				if (count($dataArr)>8) {
					break;
				}
			}


			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				$dataArr['userId']='';
				$dataArr['firstName'] = strip_tags(trim($_POST['firstName']));
				$dataArr['lastName'] = strip_tags(trim($_POST['lastName']));
				$dataArr['secondName'] = strip_tags(trim($_POST['secondName']));
				$dataArr['email'] = strip_tags(trim($_POST['email']));
				$dataArr['telephoneNumber'] = strip_tags(trim($_POST['telephoneNumber']));
				
				$dataArr['city'] = strip_tags(trim($_POST['city']));
				$dataArr['login'] = strip_tags(trim($_POST['login']));
				$dataArr['password'] = strip_tags(trim($_POST['password']));
				


				if($error = Validator:: checkUserName($dataArr['firstName'])){
					$err[0]['firstName'] = $error[0];
					$err[1]['firstName'] = $error[1];
				}

				if($error = Validator:: checkUserName($dataArr['lastName'])){
					$err[0]['lastName'] = $error[0];
					$err[1]['lastName'] = $error[1];
				}

				if($error = Validator:: checkUserName($dataArr['secondName'])){
					$err[0]['secondName'] = $error[0];
					$err[1]['secondName'] = $error[1];
				}
				if($error = Validator:: checkTelephoneNumber($dataArr['telephoneNumber'])){
					$err[0]['telephoneNumber'] = $error[0];
					$err[1]['telephoneNumber'] = $error[1];
				}
				if($error = Validator:: checkEmail($dataArr['email'])){
					$err[0]['email'] = $error[0];
					$err[1]['email'] = $error[1];
				}
				/*
				*Если форма без ошибок то отправляем данные в базу
				*/

				$checkForm = true;
				foreach ($err[0] as $key => $value) {
					if (!empty($value)){
						$checkForm = false;
					}

				}

				if ($checkForm) {

					// if(setrawcookie('student', $dataArr['pass'], time() + 360000, '/', DOMAIN,0,1)){
					// 	$this->model->addedUser($dataArr);
					// 	header('Location: http://' .DOMAIN);
					// }
						if($this->model->addedUser($dataArr)){
							header('Location: http://' .DOMAIN.'/profile/create/done');
							die;
					}					
	 }

			

			echo '<pre>';
			// print_r($this->validator->checkUser());
			print_r($dataArr);
			print_r($err);
			echo '</pre>';
		}

		require_once ROOT . '/../app/Views/profileCreate.php' ;

	}

	public function actionDone(){
				echo "<h1>Регистрация прошла успешно</h1>";
	
	}



}
?>