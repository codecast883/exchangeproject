<?php
require_once ROOT . '/../app/DB/ProfileGateway.php' ;
	class Validator{
		private $model;
		public function __construct(){
			$this->model = new ProfileGateway;

		}


	public static function checkUserName($field){
			$error = [];
		
			if (mb_strlen($field) == 0 or mb_strlen($field) > 40 or !preg_match('/^[а-яёА-ЯЁ]+$/u', $field)) {
					$error[0] = 'has-error';
					if (mb_strlen($field) == 0) {
						$error[1] = '<label class="control-label" for="inputError1">Введено пустое поле</label>';
					}elseif (mb_strlen($field) > 40) {
						$error[1] = '<label class="control-label" for="inputError1">Введено слишком много символов</label>';
					}elseif (!preg_match('/^[а-яёА-ЯЁ]+$/u', $field)) {
						$error[1] = '<label class="control-label" for="inputError1">Введено недопустимое имя</label>';
					}
					return $error;
					
				}
				return false;
		}

		public static function checkTelephoneNumber($field){
			$error = [];
		
			if (mb_strlen($field) == 0 or mb_strlen($field) > 40 or !preg_match('/^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$/', $field)) {
					$error[0] = 'has-error';
					if (mb_strlen($field) == 0) {
						$error[1] = '<label class="control-label" for="inputError1">Введено пустое поле</label>';
					}elseif (mb_strlen($field) > 40) {
						$error[1] = '<label class="control-label" for="inputError1">Введено слишком много символов</label>';
					}elseif (!preg_match('/^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$/', $field)) {
						$error[1] = '<label class="control-label" for="inputError1">Введен недопустимый формат</label>';
					}
					return $error;
					
				}
				return false;
		}

		public static function checkEmail($field){
			$error = [];
		
			if (mb_strlen($field) == 0 or mb_strlen($field) > 40 or !preg_match('/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/', $field)) {
					$error[0] = 'has-error';
					if (mb_strlen($field) == 0) {
						$error[1] = '<label class="control-label" for="inputError1">Введено пустое поле</label>';
					}elseif (mb_strlen($field) > 40) {
						$error[1] = '<label class="control-label" for="inputError1">Введено слишком много символов</label>';
					}elseif (!preg_match('/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/', $field)) {
						$error[1] = '<label class="control-label" for="inputError1">Введен недопустимый формат</label>';
					}
					return $error;
					
				}
				return false;
		}



		public static function checkString($field){
			$error = [];
		
			if (mb_strlen($field) == 0 or mb_strlen($field) > 40 or !preg_match('/^[0-9a-zA-Zа-яёА-ЯЁ]+$/u', $field)) {
					$error[0] = 'has-error';
					if (mb_strlen($field) == 0) {
						$error[1] = '<label class="control-label" for="inputError1">Введено пустое поле</label>';
					}elseif (mb_strlen($field) > 40) {
						$error[1] = '<label class="control-label" for="inputError1">Введено слишком много символов</label>';
					}elseif (!preg_match('/^[0-9a-zA-Zа-яёА-ЯЁ]+$/u', $field)) {
						$error[1] = '<label class="control-label" for="inputError1">Введено недопустимое имя</label>';
					}
					return $error;
					
				}
				return false;
		}

		public static function checkNumber($field){
			$error = [];
			
			if (mb_strlen($field) == 0 or mb_strlen($field) > 10 or !is_numeric($field)) {
					$error[0] = 'has-error';
					if (mb_strlen($field) == 0) {
						$error[1] = '<label class="control-label" for="inputError1">Введено пустое поле</label>';
					}elseif (mb_strlen($field) > 10) {
						$error[1] = '<label class="control-label" for="inputError1">Введено слишком много символов</label>';
					}elseif (!is_numeric($field)) {
						$error[1] = '<label class="control-label" for="inputError1">Введено недопустимое число</label>';
					}
					return $error;
					
				}
				return false;
		}



		public function checkUser(){
			if (isset($_COOKIE['student'])) {
				$hash = $_COOKIE['student'];
				if ($data = $this->model->findByHash($hash)) {
					return $data;
				}
			}

			return false;
		}

		

	}

?>