<!DOCTYPE html>
<html lang="ru-RU">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="/style/bootstrap.min.css">
  <link rel="stylesheet" href="/style/style.css">
  <script type="/text/javascript" src="/js/jquery-3.1.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width">
</head>




<body>

	<div class="container">
		
	  	<div class="col-md-6 ">
		  <h3>Создание профиля</h3>
		     <form role="form" action="" method="post">
		        


		             

		          <div class="form-group <?=$err[0]['firstName']?>">
				  <?=$err[1]['firstName'];?>
				  <input type="text" class="form-control" placeholder="Имя" name="firstName" value="<?=$dataArr['firstName']?>">
				 </div>

				  <div class="form-group <?=$err[0]['lastName']?>">
				  <?=$err[1]['lastName'];?>
		          <input type="text" class="form-control" placeholder="Фамилия" name="lastName" value="<?=$dataArr['lastName']?>">
		          </div>

		          <div class="form-group <?=$err[0]['secondName']?>">
				  <?=$err[1]['secondName'];?>
		          <input type="text" class="form-control" placeholder="Отчество" name="secondName" value="<?=$dataArr['secondName']?>">
		          </div>


				   <div class="form-group <?=$err[0]['telephoneNumber']?>" >
				   <?=$err[1]['telephoneNumber'];?>
		          <input type="number" class="form-control "  placeholder="Номер телефона" name="telephoneNumber" value="<?=$dataArr['telephoneNumber']?>">
		          </div>

		          <div class="form-group <?=$err[0]['email']?>">
				   <?=$err[1]['email'];?>
		          <input type="email" class="form-control" placeholder="email" name="email" value="<?=$dataArr['email']?>">
		          </div>

		           <div class="form-group <?=$err[0]['city']?>">
				   <?=$err[1]['city'];?>
		          <input type="text" class="form-control" placeholder="Город" name="city" value="<?=$dataArr['city']?>">
		          </div>

		           <div class="form-group <?=$err[0]['login']?>">
				   <?=$err[1]['login'];?>
		          <input type="text" class="form-control" placeholder="Логин" name="login" value="<?=$dataArr['login']?>">
		          </div>

		          <div class="form-group <?=$err[0]['password']?>">
				   <?=$err[1]['password'];?>
		          <input type="password" class="form-control" placeholder="Пароль" name="password" value="<?=$dataArr['password']?>">
		          </div>
				  
		        <button class="btn btn-success" >Зарегестрироваться</button>
		      </form>
		  </div>
  		</div>
	
			
</body>

</html>