<!DOCTYPE html>
<html lang="ru-RU">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="/style/bootstrap.min.css">
  <link rel="stylesheet" href="/style/style.css">
  <script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width">
</head>

<body>

	<div class="container">
		<div class="row">
  			<div class="col-md-10 col-md-offset-1">
				<nav class="navbar navbar-default" role="navigation">
				  <div class="container-fluid">
				 
				    <div class="navbar-header">
				      <a class="navbar-brand" href="#"></a>
				    </div>

				    <?php if ($data = $this->getNameUser()):?>
				    <p class="navbar-text">Вы вошли как, <b><?=$data;?></b><br><a href="#">Редактировать профиль</a></p>
				    <?php else: ?>
				     <p class="navbar-text">Добро пожаловать<br><a href = "/profile/create">Регистрация</a><p>
				     
				 <?php endif ?>

				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      
				      <form class="navbar-form navbar-left" role="search">
				        <div class="form-group">
				          <input type="text" class="form-control" placeholder="Поиск">
				        </div>
				        <button type="submit" class="btn btn-default">Найти</button>
				      </form>
				     
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
  			</div>