<?php
ob_start();
define('ROOT',dirname(__FILE__));
define('DOMAIN', $_SERVER['SERVER_NAME']);

require_once ROOT . '/../app/config.php';
require_once ROOT . '/../app/DB/DB.php';
require_once ROOT . '/../app/Components/Router.php';
require_once ROOT . '/../app/Components/Helper.php';
class Application
{
	public $router;
	public $helper;
	private static $app;

	function __construct()
	{
		
		$this->helper = new Helper;
		$this->router = new Router;
		self::$app = $this;

		
	}

	public function run(){
		$this->router->run();
	}

	public function getComponents($component){
		
		return $this->$component;
	}

	 public static function app()
    {
        return self::$app;
    }
}


$dbo = new Db;
$app = new Application;
$app->run();




 ?>
